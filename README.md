# CheckMe

A shell script used to gather checksums and verify said checksums   

## Usage

### Checksum a directory

To checksum a directory it is as simple as running the following
```
checkme checksum <folder>
```

The above command will output a special file called `.sha256sum.txt` into the directory. This file will contain all of the hash information for every last file in that directory.

### Verify a directory

To verify a directory that has been checksumed you can run
```
checkme verify <folder>
```

This will recursively check each file in the directory against the text file created by te previous command.
